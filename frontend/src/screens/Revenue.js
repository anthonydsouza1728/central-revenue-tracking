import { useEffect, useState } from "react";
import {
  Table,
  Tbody,
  Tr,
  Th,
  Td,
  Flex,
  Select,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Button,
  Thead,
} from "@chakra-ui/react";
import axios from "axios";
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

const Revenue = () => {
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [contacts, setContacts] = useState(data);
  const [search, setSearch] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [showData, setShowData] = useState(false);
  const [usdRate, setUsdRate] = useState(0); // New state to hold the USD rate
  const [selectedOption, setSelectedOption] = useState(""); // New state to hold the selected option
  const [total, setTotal] = useState(0); // New state to hold the total value

  let [searchParam] = useSearchParams();
  let redirect = searchParam.get("redirect") || "/";

  const userLogin = useSelector((state) => state.userLogin);
  const { loading, error, userInfo } = userLogin;

  useEffect(() => {
    if (!userInfo) {
      navigate(redirect);
    }
    revenueData();
    fetchUsdRate(); // Fetch the USD rate on component mount
  }, []);

  const revenueData = async () => {
    try {
      const { data } = await axios.get("/api/jsonData");
      setData(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchUsdRate = async () => {
    try {
      const { data } = await axios.get("https://bitpay.com/api/rates/BTC/USD");
      const currentUsd = data.rate;
      setUsdRate(currentUsd); // Update the USD rate state with the fetched value
    } catch (error) {
      console.log(error);
    }
  };

  const handleViewClick = async () => {
    try {
      await fetchUsdRate();
      setShowData(selectedOption === "Bitstarz" && startDate && endDate);
    } catch (error) {
      console.log(error);
    }
  };

  const handleOptionChange = (e) => {
    setSelectedOption(e.target.value);
  };

  const convertToUSD = (btcValue) => {
    return btcValue * usdRate; // Calculate the USD value using the fetched USD rate
  };

  const calculate40Percent = (usdValue) => {
    return usdValue * 0.4;
  };

  useEffect(() => {
    // Calculate the total value whenever the filtered data changes
    const totalValue = data
      .filter((item) => {
        return (
          selectedOption === "Bitstarz" &&
          item["name"] === "Bitstarz" &&
          item.Date >= startDate &&
          item.Date <= endDate + 1
        );
      })
      .reduce((sum, item) => {
        const btcValue = item["BTC NGR"];
        const usdValue = convertToUSD(btcValue);
        const percent40 = calculate40Percent(usdValue);
        return sum + percent40;
      }, 0);
    setTotal(totalValue);
  }, [data, selectedOption, startDate, endDate]);

  return (
    <>
      <Flex ml="10px" mr="10px">
        <Select
          placeholder="Select option"
          w="500px"
          border="solid black"
          mt="32px"
          id="dataTypeInput"
          value={selectedOption}
          onChange={handleOptionChange}
        >
          <option value="Overall">Overall (Don't select any date)</option>
          <option value="Bitstarz">Bitstarz</option>
          <option value="Crytowild">Crytowild</option>
          <option value="Cointiply">Cointiply</option>
          <option value="Lincoln">Lincoln</option>
          <option value="Intertops">Intertops</option>
          <option value="CafeCasino">CafeCasino and Slot.lv</option>
        </Select>

        <FormControl w="500px" ml="10px" mr="10px" id="fromDateInput">
          <FormLabel>Select Date:</FormLabel>
          <Input
            type="date"
            border="solid black"
            required
            value={startDate}
            onChange={(e) => setStartDate(e.target.value)}
            placeholder="Start Date"
            min="2020-04-01"
            max="2020-07-20"
          />
          <FormHelperText>Please select the Date</FormHelperText>
        </FormControl>
        <FormControl w="500px" ml="10px" mr="10px" id="toDateInput">
          <FormLabel>To</FormLabel>
          <Input
            type="date"
            border="solid black"
            required
            value={endDate}
            onChange={(e) => setEndDate(e.target.value)}
            placeholder="End Date"
            min={startDate}
            max="2020-07-20"
          />
          <FormHelperText>Please select the Date till</FormHelperText>
        </FormControl>
        <Button
          colorScheme="blue"
          w="100px"
          ml="10px"
          mt="30px"
          id="viewButton"
          onClick={handleViewClick}
        >
          View
        </Button>
      </Flex>
      {showData && (
        <Table variant="striped">
          <Thead>
            <Tr>
              {/* <Th>Type</Th> */}
              <Th>Date</Th>
              <Th>Brand ID</Th>
              <Th>Visits count</Th>
              <Th>Registrations count</Th>
              <Th>BTC First deposits count</Th>
              <Th>BTC Deposits sum</Th>
              <Th>BTC NGR</Th>
              <Th>In USD</Th>
              <Th>40 percent</Th>
            </Tr>
          </Thead>
          <Tbody>
            {data
              .filter((item) => {
                return (
                  selectedOption === "Bitstarz" &&
                  item["name"] === "Bitstarz" &&
                  item.Date >= startDate &&
                  item.Date <= endDate + 1
                );
              })
              .filter((item) => {
                return search === "" ? true : item.Date.includes(search);
              })
              .map((item, index) => {
                const btcValue = item["BTC NGR"];
                const usdValue = convertToUSD(btcValue);
                const percent40 = calculate40Percent(usdValue);

                return (
                  <Tr key={index}>
                    {/* <Td>{item["name"]}</Td> */}
                    <Td>{item.Date}</Td>
                    <Td>{item["Brand ID"]}</Td>
                    <Td>{item["Visits count"]}</Td>
                    <Td>{item["Registrations count"]}</Td>
                    <Td>{item["BTC First deposits count"]}</Td>
                    <Td>{item["BTC Deposits sum"]}</Td>
                    <Td>{item["BTC NGR"]}</Td>
                    <Td>{usdValue.toFixed(2)}</Td>
                    <Td>{percent40.toFixed(2)}</Td>
                  </Tr>
                );
              })}
            <Tr>
              <Td></Td>
              <Td></Td>
              <Td></Td>
              <Td></Td>
              <Td></Td>
              <Td></Td>
              <Td></Td>
              <Td fontWeight="bold" textTransform="capitalize">
                Total:
              </Td>
              <Td textAlign="right" fontWeight="bold">
                {total.toFixed(2)}
              </Td>
            </Tr>
          </Tbody>
        </Table>
      )}
    </>
  );
};

export default Revenue;
