import { Icon, MenuButton } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

function MenuSingleButton({ icon, label }) {
  return (
    <MenuButton
      as={RouterLink}
      colorScheme="gray.800"
      ml="-3.5"
      letterSpacing="wide"
      color="white"
      fontWeight="bold"
      textTransform="uppercase"
      _hover={{ color: "cyan" }}
    >
      <Icon as={icon} mr="2" w="4" h="4" />
      {label}
    </MenuButton>
  );
}

export default MenuSingleButton;
