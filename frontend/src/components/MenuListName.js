import { Link, MenuGroup } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

const MenuListName = ({ url, label }) => {
  return (
    <>
      <Link
        as={RouterLink}
        to={url}
        //   fontSize="sm"
        letterSpacing="wide"
        color="white"
        fontWeight="bold"
        textTransform="uppercase"
        mr="5"
        display="flex"
        alignItems="center"
        wrap="wrap"
        _hover={{ color: "cyan" }}
        mt={{ base: 4, md: 0 }}
      >
        <MenuGroup title={label}></MenuGroup>
      </Link>
    </>
  );
};

export default MenuListName;
