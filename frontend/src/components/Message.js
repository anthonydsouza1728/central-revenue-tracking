import Alert from "react-bootstrap/Alert";

const Message = ({ type = "info", children }) => {
  return <Alert variant={type}>{children}</Alert>;
};

export default Message;
