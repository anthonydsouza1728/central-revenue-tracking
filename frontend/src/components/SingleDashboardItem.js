import { Link } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

const SingleDashboardItem = ({ url, icon, label }) => {
  return (
    <Link
      as={RouterLink}
      to={url}
      //   fontSize="sm"
      letterSpacing="wide"
      color="white"
      fontWeight="bold"
      textTransform="uppercase"
      mr="5"
      display="flex"
      alignItems="center"
      _hover={{ color: "cyan" }}
      mt={{ base: 4, md: 0 }}
    >
      {icon}
      {label}
    </Link>
  );
};

export default SingleDashboardItem;
