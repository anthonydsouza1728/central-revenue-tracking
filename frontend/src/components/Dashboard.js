import {
  Flex,
  Icon,
  Link,
  List,
  Menu,
  MenuList,
  MenuDivider,
} from "@chakra-ui/react";
import {
  AiOutlineDashboard,
  AiFillProfile,
  AiOutlineLogout,
} from "react-icons/ai";
import { BsDatabaseAdd } from "react-icons/bs";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { PiUserListBold } from "react-icons/pi";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";

import SingleDashboardItem from "./SingleDashboardItem";
import { logout } from "../actions/userActions";
import MenuListName from "./MenuListName";
import MenuSingleButton from "./MenuSingleButton";

const Dashboard = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const logoutHandler = () => {
    dispatch(logout());
    navigate("/");
  };

  return (
    <Flex
      flexDirection="column"
      justifyContent="flex-start"
      alignContent="flex-start"
      gap="5%"
      wrap="wrap"
      py="12"
      px="6"
      bgColor="#1C4E80"
      w="20%"
      h="100vh"
      pos="fixed"
      as="h2"
      color="white"
      fontWeight="bold"
      fontSize="md"
      letterSpacing="wide"
    >
      <List spacing={10}>
        <SingleDashboardItem
          url="/dashboard"
          label="Dashboard"
          icon={<Icon as={AiOutlineDashboard} mr="1" w="4" h="4" />}
        />

        <SingleDashboardItem
          url="/profile"
          label="Profile"
          icon={<Icon as={AiFillProfile} mr="1" w="4" h="4" />}
        />

        <SingleDashboardItem
          url="/admin/userlist"
          label="Userlist"
          icon={<Icon as={PiUserListBold} mr="1" w="4" h="4" />}
        />

        <SingleDashboardItem
          url="/dashboard"
          label="Overall Data"
          icon={<Icon as={BsDatabaseAdd} mr="1" w="4" h="4" />}
        />

        <Link
          letterSpacing="wide"
          color="white"
          fontWeight="bold"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          wrap="wrap"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
        >
          <Menu>
            <MenuSingleButton icon="BiLogoBitcoin" label="Bitcoin Casino" />
            <MenuList bgColor="gray.800" color="whiteAlpha.800">
              <MenuListName url="/dashboard" label="Cryptowild Casino" />
              <MenuDivider />
              <MenuListName url="/dashboard" label="Bitstarz Casino" />
            </MenuList>
          </Menu>
        </Link>

        <Link
          as={RouterLink}
          to="/admin/userlist"
          letterSpacing="wide"
          color="white"
          fontWeight="bold"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          wrap="wrap"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
        >
          <Menu>
            <MenuSingleButton icon="MdCasino" label="Real Money Casino" />

            <MenuList bgColor="gray.800" color="whiteAlpha.800">
              <MenuListName url="/dashboard" label="Lincoln Casino" />
              <MenuDivider />
              <MenuListName url="/dashboard" label="Intertops Casino" />
              <MenuDivider />
              <MenuListName
                url="/dashboard"
                label="CafeCasino and Slots.lv Casino"
              />
            </MenuList>
          </Menu>
        </Link>

        <Link
          as={RouterLink}
          to="/"
          letterSpacing="wide"
          color="white"
          fontWeight="bold"
          textTransform="uppercase"
          mr="5"
          display="flex"
          alignItems="center"
          _hover={{ color: "cyan" }}
          mt={{ base: 4, md: 0 }}
          onClick={logoutHandler}
        >
          <Icon as={AiOutlineLogout} mr="1" w="4" h="4" />
          Logout
        </Link>
      </List>
    </Flex>
  );
};

export default Dashboard;
