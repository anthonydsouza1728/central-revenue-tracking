import { Flex, Text } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Flex
      as="footer"
      justifyContent="center"
      py="5"
      bgColor="#1C4E80"
      zIndex="9999"
    >
      <Text color="whiteAlpha.800">
        Copyright {new Date().getFullYear()} Central Revenue Tracking App. All
        Rights Reserved.
      </Text>
    </Flex>
  );
};

export default Footer;
