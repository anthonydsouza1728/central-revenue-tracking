import mongoose from "mongoose";

const revenueSchema = mongoose.Schema(
  {
    date: {
      type: Date,
      require: true,
    },
    brandId: {
      type: Number,
      require: true,
    },
    visitCount: {
      type: Number,
      require: true,
    },
    registrationsCount: {
      type: Number,
      require: true,
    },
    btcFirstDepositsCount: {
      type: Number,
      require: true,
    },
    btcDepositsSum: {
      type: Number,
      require: true,
    },
    btcNGR: {
      type: Number,
      require: true,
    },
    usdAmount: {
      type: Number,
      require: true,
    },
    usdPercentage: {
      type: Number,
      require: true,
    },
    casinoName: {
      type: String,
      require: true,
    },
  },
  { timestamps: true }
);

const Revenue = mongoose.model("Revenue", revenueSchema);

export default Revenue;
