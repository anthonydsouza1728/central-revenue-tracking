import json
json_data = [
    {
        "Date": "2020-04-01 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 61,
        "Registrations count": 20,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00000988,
        "BTC NGR": -0.001224405
    },
    {
        "Date": "2020-04-02 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 73,
        "Registrations count": 32,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.00805551,
        "BTC NGR": 0.0083383425
    },
    {
        "Date": "2020-04-03 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 91,
        "Registrations count": 36,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.0010382325
    },
    {
        "Date": "2020-04-04 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 95,
        "Registrations count": 33,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.0036951225
    },
    {
        "Date": "2020-04-05 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 85,
        "Registrations count": 25,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.0005,
        "BTC NGR": -0.000385335
    },
    {
        "Date": "2020-04-06 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 80,
        "Registrations count": 29,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.001395825
    },
    {
        "Date": "2020-04-07 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 86,
        "Registrations count": 31,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.00072165
    },
    {
        "Date": "2020-04-08 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 55,
        "Registrations count": 15,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.0019335825
    },
    {
        "Date": "2020-04-09 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 82,
        "Registrations count": 33,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.00074505
    },
    {
        "Date": "2020-04-10 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 78,
        "Registrations count": 19,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.00044682
    },
    {
        "Date": "2020-04-11 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 82,
        "Registrations count": 23,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.0002,
        "BTC NGR": -0.0006528
    },
    {
        "Date": "2020-04-12 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 83,
        "Registrations count": 22,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.0062,
        "BTC NGR": -0.003823515
    },
    {
        "Date": "2020-04-13 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 108,
        "Registrations count": 37,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.00756525
    },
    {
        "Date": "2020-04-14 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 84,
        "Registrations count": 36,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": -0.0022990275
    },
    {
        "Date": "2020-04-15 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 85,
        "Registrations count": 35,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": -0.001038495
    },
    {
        "Date": "2020-04-16 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 87,
        "Registrations count": 39,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": -0.0009101625
    },
    {
        "Date": "2020-04-17 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 84,
        "Registrations count": 24,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.0003788475
    },
    {
        "Date": "2020-04-18 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 75,
        "Registrations count": 38,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01531989,
        "BTC NGR": -0.0245766675
    },
    {
        "Date": "2020-04-19 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 101,
        "Registrations count": 35,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01508779,
        "BTC NGR": -0.01204353
    },
    {
        "Date": "2020-04-20 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 78,
        "Registrations count": 22,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.00380465,
        "BTC NGR": 0.0031651125
    },
    {
        "Date": "2020-04-21 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 69,
        "Registrations count": 34,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.03117,
        "BTC NGR": -0.00634023
    },
    {
        "Date": "2020-04-22 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 80,
        "Registrations count": 28,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.0024145125
    },
    {
        "Date": "2020-04-23 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 77,
        "Registrations count": 27,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.015,
        "BTC NGR": -0.001657965
    },
    {
        "Date": "2020-04-24 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 81,
        "Registrations count": 38,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.0177544275
    },
    {
        "Date": "2020-04-25 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 61,
        "Registrations count": 20,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": -0.00126441
    },
    {
        "Date": "2020-04-26 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 70,
        "Registrations count": 33,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.00332523,
        "BTC NGR": -0.001305255
    },
    {
        "Date": "2020-04-27 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 69,
        "Registrations count": 27,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.0041,
        "BTC NGR": 0.006633195
    },
    {
        "Date": "2020-04-28 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 102,
        "Registrations count": 34,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.02482847,
        "BTC NGR": 0.0066502575
    },
    {
        "Date": "2020-04-29 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 53,
        "Registrations count": 21,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.01100162,
        "BTC NGR": 0.0006433425
    },
    {
        "Date": "2020-04-30 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 109,
        "Registrations count": 42,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.02186797,
        "BTC NGR": 0.006846405
    },
    {
        "Date": "2020-05-01 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 113,
        "Registrations count": 58,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00421348,
        "BTC NGR": 0.003899895
    },
    {
        "Date": "2020-05-02 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 128,
        "Registrations count": 51,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.01561364,
        "BTC NGR": 0.000672195
    },
    {
        "Date": "2020-05-03 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 54,
        "Registrations count": 27,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01,
        "BTC NGR": 0.0120067425
    },
    {
        "Date": "2020-05-04 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 113,
        "Registrations count": 43,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00964608,
        "BTC NGR": -0.008662125
    },
    {
        "Date": "2020-05-05 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 125,
        "Registrations count": 42,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": -0.0002067375
    },
    {
        "Date": "2020-05-06 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 111,
        "Registrations count": 38,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.02574479,
        "BTC NGR": 0.007729065
    },
    {
        "Date": "2020-05-07 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 96,
        "Registrations count": 37,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.02020432,
        "BTC NGR": 0.009614445
    },
    {
        "Date": "2020-05-08 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 99,
        "Registrations count": 19,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00396548,
        "BTC NGR": -0.001210125
    },
    {
        "Date": "2020-05-09 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 82,
        "Registrations count": 29,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01995788,
        "BTC NGR": 0.0284977125
    },
    {
        "Date": "2020-05-10 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 117,
        "Registrations count": 33,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.02203656,
        "BTC NGR": -0.0179178675
    },
    {
        "Date": "2020-05-11 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 98,
        "Registrations count": 29,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.02850759,
        "BTC NGR": -0.0301284675
    },
    {
        "Date": "2020-05-12 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 114,
        "Registrations count": 23,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": -0.021222765
    },
    {
        "Date": "2020-05-13 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 86,
        "Registrations count": 26,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.04480792,
        "BTC NGR": -0.14043006
    },
    {
        "Date": "2020-05-14 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 93,
        "Registrations count": 14,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.12324353,
        "BTC NGR": 0.078097785
    },
    {
        "Date": "2020-05-15 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 97,
        "Registrations count": 17,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.2154748,
        "BTC NGR": 0.1708512225
    },
    {
        "Date": "2020-05-16 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 104,
        "Registrations count": 25,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.07811258,
        "BTC NGR": 0.0587097825
    },
    {
        "Date": "2020-05-17 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 87,
        "Registrations count": 16,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.0250918,
        "BTC NGR": 0.0041540025
    },
    {
        "Date": "2020-05-18 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 111,
        "Registrations count": 25,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.0244326,
        "BTC NGR": 0.02404329
    },
    {
        "Date": "2020-05-19 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 84,
        "Registrations count": 22,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01419994,
        "BTC NGR": 0.01634457
    },
    {
        "Date": "2020-05-20 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 86,
        "Registrations count": 24,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00155256,
        "BTC NGR": 0.00767055
    },
    {
        "Date": "2020-05-21 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 103,
        "Registrations count": 20,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00466375,
        "BTC NGR": 0.0200621775
    },
    {
        "Date": "2020-05-22 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 92,
        "Registrations count": 13,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00130742,
        "BTC NGR": -0.01009647
    },
    {
        "Date": "2020-05-23 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 74,
        "Registrations count": 14,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00043691,
        "BTC NGR": 0.00562455
    },
    {
        "Date": "2020-05-24 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 92,
        "Registrations count": 26,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01569361,
        "BTC NGR": 0.012865935
    },
    {
        "Date": "2020-05-25 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 97,
        "Registrations count": 22,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.01258003,
        "BTC NGR": 0.01025208
    },
    {
        "Date": "2020-05-26 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 94,
        "Registrations count": 37,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01283523,
        "BTC NGR": 0.0084007275
    },
    {
        "Date": "2020-05-27 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 101,
        "Registrations count": 33,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.03532276,
        "BTC NGR": 0.0266843625
    },
    {
        "Date": "2020-05-28 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 230,
        "Registrations count": 79,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.01774613,
        "BTC NGR": 0.0041491575
    },
    {
        "Date": "2020-05-29 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 131,
        "Registrations count": 53,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00350759,
        "BTC NGR": 0.0113228475
    },
    {
        "Date": "2020-05-30 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 114,
        "Registrations count": 49,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00421774,
        "BTC NGR": 0.001440975
    },
    {
        "Date": "2020-05-31 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 117,
        "Registrations count": 42,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00392466,
        "BTC NGR": -0.0035449125
    },
    {
        "Date": "2020-06-01 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 284,
        "Registrations count": 112,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.00719885,
        "BTC NGR": 0.0062142975
    },
    {
        "Date": "2020-06-02 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 165,
        "Registrations count": 63,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00210061,
        "BTC NGR": 0.00030624
    },
    {
        "Date": "2020-06-03 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 127,
        "Registrations count": 65,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.00543594,
        "BTC NGR": 0.0040824525
    },
    {
        "Date": "2020-06-04 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 107,
        "Registrations count": 37,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.0021268,
        "BTC NGR": 0.00090447
    },
    {
        "Date": "2020-06-05 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 107,
        "Registrations count": 44,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00641045,
        "BTC NGR": 0.00251199
    },
    {
        "Date": "2020-06-06 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 110,
        "Registrations count": 41,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.00346046,
        "BTC NGR": 0.0051905025
    },
    {
        "Date": "2020-06-07 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 108,
        "Registrations count": 27,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.0025,
        "BTC NGR": 0.002357895
    },
    {
        "Date": "2020-06-08 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 121,
        "Registrations count": 35,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.0101,
        "BTC NGR": -0.00163473
    },
    {
        "Date": "2020-06-09 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 128,
        "Registrations count": 31,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.0026796525
    },
    {
        "Date": "2020-06-10 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 105,
        "Registrations count": 37,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": -0.000235875
    },
    {
        "Date": "2020-06-11 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 99,
        "Registrations count": 51,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.00047338,
        "BTC NGR": -0.002928945
    },
    {
        "Date": "2020-06-12 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 99,
        "Registrations count": 38,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00119064,
        "BTC NGR": -0.00419568
    },
    {
        "Date": "2020-06-13 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 94,
        "Registrations count": 29,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.01861219,
        "BTC NGR": 0.0108060525
    },
    {
        "Date": "2020-06-14 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 91,
        "Registrations count": 36,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01133574,
        "BTC NGR": 0.0104130225
    },
    {
        "Date": "2020-06-15 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 93,
        "Registrations count": 37,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00159904,
        "BTC NGR": 0.0005407125
    },
    {
        "Date": "2020-06-16 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 96,
        "Registrations count": 33,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00159904,
        "BTC NGR": 0.001391205
    },
    {
        "Date": "2020-06-17 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 82,
        "Registrations count": 33,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00904591,
        "BTC NGR": -0.0219876375
    },
    {
        "Date": "2020-06-18 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 83,
        "Registrations count": 29,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00628348,
        "BTC NGR": -0.0032100375
    },
    {
        "Date": "2020-06-19 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 65,
        "Registrations count": 27,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.01442103,
        "BTC NGR": -0.0431391675
    },
    {
        "Date": "2020-06-20 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 78,
        "Registrations count": 29,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.01030732,
        "BTC NGR": 0.007901205
    },
    {
        "Date": "2020-06-21 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 80,
        "Registrations count": 28,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.03979363,
        "BTC NGR": 0.0096833475
    },
    {
        "Date": "2020-06-22 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 101,
        "Registrations count": 33,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.02,
        "BTC NGR": -0.0740236575
    },
    {
        "Date": "2020-06-23 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 98,
        "Registrations count": 40,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.05970478,
        "BTC NGR": 0.010722795
    },
    {
        "Date": "2020-06-24 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 94,
        "Registrations count": 39,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.07347084
    },
    {
        "Date": "2020-06-25 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 86,
        "Registrations count": 32,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00447534,
        "BTC NGR": -0.0171369375
    },
    {
        "Date": "2020-06-26 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 80,
        "Registrations count": 29,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00044541,
        "BTC NGR": 0.03859419
    },
    {
        "Date": "2020-06-27 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 81,
        "Registrations count": 26,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00470107,
        "BTC NGR": 0.030347235
    },
    {
        "Date": "2020-06-28 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 80,
        "Registrations count": 29,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.08803157,
        "BTC NGR": 0.055184205
    },
    {
        "Date": "2020-06-29 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 114,
        "Registrations count": 37,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00123446,
        "BTC NGR": 0.02199267
    },
    {
        "Date": "2020-06-30 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 86,
        "Registrations count": 27,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.02329443,
        "BTC NGR": -0.007916355
    },
    {
        "Date": "2020-07-01 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 66,
        "Registrations count": 18,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.02846249,
        "BTC NGR": 0.003050355
    },
    {
        "Date": "2020-07-02 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 57,
        "Registrations count": 22,
        "BTC First deposits count": 2,
        "BTC Deposits sum": 0.0196969,
        "BTC NGR": 0.01196925
    },
    {
        "Date": "2020-07-03 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 56,
        "Registrations count": 25,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00573412,
        "BTC NGR": 0.002990175
    },
    {
        "Date": "2020-07-04 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 70,
        "Registrations count": 21,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.0048088425
    },
    {
        "Date": "2020-07-05 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 49,
        "Registrations count": 23,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": -0.007915875
    },
    {
        "Date": "2020-07-06 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 122,
        "Registrations count": 37,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00203,
        "BTC NGR": 0.012053175
    },
    {
        "Date": "2020-07-07 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 81,
        "Registrations count": 26,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.05820775,
        "BTC NGR": 0.002717535
    },
    {
        "Date": "2020-07-08 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 73,
        "Registrations count": 20,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00107833,
        "BTC NGR": 0.0166599075
    },
    {
        "Date": "2020-07-09 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 76,
        "Registrations count": 27,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.03325137,
        "BTC NGR": 0.0240456675
    },
    {
        "Date": "2020-07-10 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 62,
        "Registrations count": 25,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.00105411
    },
    {
        "Date": "2020-07-11 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 63,
        "Registrations count": 28,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00179281,
        "BTC NGR": 0.0014433075
    },
    {
        "Date": "2020-07-12 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 63,
        "Registrations count": 17,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.05449168,
        "BTC NGR": -0.000923625
    },
    {
        "Date": "2020-07-13 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 75,
        "Registrations count": 33,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.01823857,
        "BTC NGR": 0.0267704175
    },
    {
        "Date": "2020-07-14 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 54,
        "Registrations count": 17,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.00528217,
        "BTC NGR": 0.0122895
    },
    {
        "Date": "2020-07-15 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 75,
        "Registrations count": 20,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00897547,
        "BTC NGR": -0.001104195
    },
    {
        "Date": "2020-07-16 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 56,
        "Registrations count": 16,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.01412395,
        "BTC NGR": 0.015434055
    },
    {
        "Date": "2020-07-17 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 65,
        "Registrations count": 13,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.03058806,
        "BTC NGR": 0.0003993375
    },
    {
        "Date": "2020-07-18 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 71,
        "Registrations count": 31,
        "BTC First deposits count": 1,
        "BTC Deposits sum": 0.00385576,
        "BTC NGR": 0.0124670175
    },
    {
        "Date": "2020-07-19 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 77,
        "Registrations count": 28,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0.02294669,
        "BTC NGR": 0.0199988325
    },
    {
        "Date": "2020-07-20 00:00:00 UTC",
        "Brand ID": 1,
        "Visits count": 17,
        "Registrations count": 5,
        "BTC First deposits count": 0,
        "BTC Deposits sum": 0,
        "BTC NGR": 0.002980125
    }
]

# Output the variable value in JSON format
output = {"variable": json_data}
print(json.dumps(output))
